<?php
  $url = "https://bitbucket.org/yournamehere/profile/activity";
  function curl_load($url){
    curl_setopt($ch=curl_init(), CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }
  
  $dom = new DOMDocument();
  $html = curl_load($url);
  $dom->validateOnParse = true; //<!-- this first
  $dom->loadHTML($html);        //'cause 'load' == 'parse
  $dom->preserveWhiteSpace = FALSE;
  $article = $dom->getElementsByTagName('article');
  echo '<ul>';
  
  foreach ($article as $article) {
    $article = preg_replace('!\s+!', ' ', $article->nodeValue);  
    $article = preg_replace('((\d{4})-(\d{2})-(\d{2}))', '<br/>', $article);    
    echo '<li>', $article, '</li>';
  }
  echo '</ul>';
?>